function reset() {
    document.getElementById("content").innerHTML = '';
}

function submit() {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost/api/payment/initiate", true);
    xhr.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById("content").innerHTML = this.responseText;
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(
        {
            "bankAccountNumber": "001346097288",
            "amount":"100.0"
        }));
}