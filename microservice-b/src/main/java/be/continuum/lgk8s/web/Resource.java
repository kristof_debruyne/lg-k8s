package be.continuum.lgk8s.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.springframework.http.HttpMethod.GET;

@RestController
@RequestMapping("/api/payment")
@Slf4j
public class Resource {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${microservice.c.url}")
    private String url;

    @PostMapping("/process")
    public ResponseEntity<String> processPayment(@RequestBody PaymentProcessRequest request) {
        log.info("Processing payment with amount [ {} ] on account [ {} ] ...", request.getAmount(), request.getBankAccountNumber());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        URI uri = UriComponentsBuilder.fromUriString(url)
                .buildAndExpand(request.getUuid().toString())
                .toUri();

        return restTemplate.exchange(uri, GET, new HttpEntity<>(request, headers), String.class);
    }
}