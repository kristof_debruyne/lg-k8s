#!/bin/sh

echo "Starting MicroService B"

set -e

if [ "${DEBUG:-false}" = "true" ]
then
    DEBUG_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=${DEBUG_PORT}"
fi

if [ -z ${JAVA_OPTS+x} ]
then
    JAVA_OPTS="-Xmn512 -Xms1024m -Xmx1024m";
fi

export DUMPS_PATH="/app/dump"
mkdir -p ${DUMPS_PATH}

exec java \
    ${DEBUG_OPTS} \
    ${JAVA_OPTS} \
    -XX:+PrintFlagsFinal \
    -XX:+UnlockExperimentalVMOptions \
    -XX:+UseCGroupMemoryLimitForHeap \
    -XX:+HeapDumpOnOutOfMemoryError \
    -XX:HeapDumpPath=${DUMPS_PATH} \
    -XX:OnOutOfMemoryError="kill -3 %p; sleep 2; kill %p; sleep 2; kill -9 %p" \
    -Djava.security.egd=file:/dev/./urandom \
    -Dspring.profiles.active=default \
    -jar /app/app.jar