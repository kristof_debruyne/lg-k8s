#!/usr/bin/env bash
set -e

docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):$(which docker) -v $HOME/.m2:/root/.m2 -v $(pwd):/opt/app openjdk:8-jdk-slim-buster sh -c "cd /opt/app; ./mvnw clean install -Pdocker; chown -R $(id -u) /opt/app"

cd client
docker build -t lgk8s/client:1.0 .
cd ..