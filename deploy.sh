#!/usr/bin/env bash
set -e

kubectl apply -f cluster/database/replicaset.yaml
kubectl apply -f cluster/database/service.yaml

kubectl apply -f cluster/microservice-a/replicaset.yaml
kubectl apply -f cluster/microservice-a/service.yaml
kubectl apply -f cluster/microservice-a/ingress.yaml

kubectl apply -f cluster/microservice-b/replicaset.yaml
kubectl apply -f cluster/microservice-b/service.yaml

kubectl apply -f cluster/microservice-c/replicaset.yaml
kubectl apply -f cluster/microservice-c/service.yaml

kubectl apply -f cluster/client/replicaset.yaml
kubectl apply -f cluster/client/service.yaml
kubectl apply -f cluster/client/ingress.yaml

echo "Installing contour ingress controller ..."
kubectl apply -f https://j.hept.io/contour-deployment-rbac
kubectl get -n projectcontour service contour -o wide
